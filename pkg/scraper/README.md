Scraper for [RARBG](https://rarbgunblock.com/torrents.php).
 
* Able to detect threat safety page checking user browser (it should not normally
happen because cookies are set correcty, but in case of too many requests...)
* Capable of decoding the image presented in the threat safety page