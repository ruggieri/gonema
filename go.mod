module gitlab.com/ruggieri/gonema

go 1.12

require (
	github.com/chromedp/cdproto v0.0.0-20190908230719-576f726fb49f
	github.com/chromedp/chromedp v0.4.0
	github.com/davecgh/go-spew v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/nmmh/magneturi v0.0.0-20180607142838-464c677b3fb5
	github.com/otiai10/gosseract v2.2.1+incompatible
	github.com/otiai10/mint v1.3.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20190912160710-24e19bdeb0f2 // indirect
)
